#include "FPS_GT511C3.h"
#include "SoftwareSerial.h"

FPS_GT511C3 fps(4, 5); // (Arduino SS_RX = pin 4, Arduino SS_TX = pin 5)

enum maquina_estados {
  INICIO,
  ESPERA_INPUT,
  LER_DEDO,
  DELETAR_TUDO,
  IMPRIME_ID,
  FAZ_A,
  FAZ_B,
  GRAVAR_DEDO,
  GRAVAR_ID
  };
 
enum maquina_estados estado;
  int gravar_id = 0;
  int op=0;
  int id=0;
  String opcao;

String ReadStringSerial()
{
  String conteudo = "";
  char caractere;
  
  // Enquanto receber algo pela serial
  while(Serial.available() > 0) 
  {
    // Lê byte da serial
    caractere = Serial.read();
    // Ignora caractere de quebra de linha
    if (caractere != '\n'){
      // Concatena valores
      conteudo.concat(caractere);
    }
    // Aguarda buffer serial ler próximo caractere
    delay(10);
  } 
  return conteudo;
}

void Enroll()
{
  // Enroll test

  // find open enroll id
  int enrollid = 0;
  bool usedid = true;
  while (usedid == true)
  {
    usedid = fps.CheckEnrolled(enrollid);
    if (usedid==true) enrollid++;
  }
  fps.EnrollStart(enrollid);

  // enroll
  Serial.print("Pressione o dedo para inscrever a digital #");
  Serial.println(enrollid);
  while(fps.IsPressFinger() == false) delay(100);
  bool bret = fps.CaptureFinger(true);
  int iret = 0;
  if (bret != false)
  {
    Serial.println("Remova o dedo");
    fps.Enroll1(); 
    while(fps.IsPressFinger() == true) delay(100);
    Serial.println("Pressione o mesmo dedo novamente");
    while(fps.IsPressFinger() == false) delay(100);
    bret = fps.CaptureFinger(true);
    if (bret != false)
    {
      Serial.println("Reomva o dedo");
      fps.Enroll2();
      while(fps.IsPressFinger() == true) delay(100);
      Serial.println("Pressione o mesmo dedo mais uma vez");
      while(fps.IsPressFinger() == false) delay(100);
      bret = fps.CaptureFinger(true);
      if (bret != false)
      {
        Serial.println("Remova o dedo");
        iret = fps.Enroll3();
        if (iret == 0)
        {
          Serial.println("Inscrição com Sucesso");
        }
        else
        {
          Serial.print("Falha ao inscrever a digital! error code: ");
          Serial.println(iret);
        }
      }
      else Serial.println("Falha ao capturar a digital na terceira tentativa");
    }
    else Serial.println("Falha ao capturar a digital na segunda tentativa");
  }
  else Serial.println("Falha ao capturar a digiutal na primeira tentiva");
}

void ler_dedo()
{

}

void setup()
{
  Serial.begin(9600); //set up Arduino's hardware serial UART
  delay(100);
  fps.Open();         //send serial command to initialize fps
  fps.SetLED(true);   //turn on LED so fps can see fingerprint
  estado = INICIO;
}

void loop()
{

  switch (estado){
    case INICIO:
      Serial.println("1- LER DEDO \t 2- GRAVAR DEDO \t 3- DELETAR TUDO");
      estado = ESPERA_INPUT;
      gravar_id = 0;
      op = 0;
	    id = 0;
      fps.SetLED(false);
      break;
    case ESPERA_INPUT:
      //Serial.println("1- LER DEDO \t 2- GRAVAR DEDO");
      opcao = ReadStringSerial();
      op = atoi(opcao.c_str() );
      if(op == 1)
      {
        estado = LER_DEDO;
      }
      else if(op == 2)
      {
        estado = GRAVAR_DEDO;
      }
      else if(op == 3)
      {
        estado = DELETAR_TUDO;
      }
      else
        estado = ESPERA_INPUT;
      break;
    case LER_DEDO:
        fps.SetLED(true);
        // Identify fingerprint test
        if (fps.IsPressFinger())
        {
          fps.CaptureFinger(false);
           id = fps.Identify1_N();
          if (id <20) //<- change id value depending model you are using
          {//if the fingerprint matches, provide the matching template ID
            estado = IMPRIME_ID;
          }
          else
          {//if unable to recognize
            estado = FAZ_B;
          }
        }
        else
        {
          Serial.println("Por favor pressione o dedo");
        }
        delay(100);
      break;
    case IMPRIME_ID:
          Serial.print("ID: ");
          Serial.println(id);
          if(gravar_id == 1)
            Serial.println("Digital já cadastrada no banco");
          estado = FAZ_A;
      break;
    case FAZ_A:
          //o que fazer
          estado = INICIO;
      break;
    case FAZ_B:
      Serial.println("Dedo nao encontrado");
      if(gravar_id == 1){
        Serial.println("Iniciando processo de inscrição");
        estado = GRAVAR_ID;
      }
      else{
        estado = INICIO;
      }
      break;
    case GRAVAR_DEDO:
      gravar_id = 1;
      estado = LER_DEDO;
      break;
    case GRAVAR_ID:
      Enroll();
      estado = INICIO;
      break;
    case DELETAR_TUDO:
      fps.DeleteAll();
      estado = INICIO;
      break;
  }
}
